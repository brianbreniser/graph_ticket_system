TODO file

... Gosh, I wish I had a lightweight software design app I could use to design and create work items for... oh... right...

Architecture:

Things displayed to screen
   ^
   |
Presenter objects
   ^
   |
Presenter data
   ^
   |
Entity
   ^
   |
Database

There isn't a lot of business logic (yet) for the core of the application. It will mostly store/retrieve the data structure and present it
Once I get users/web going, then we can think about more complicated logic

TODO: Features:
- Node - A generic object that can contain features, work items (tickets), or details
    - all nodes:
        - title
        - description
        - comments
        - tooltip
    - feature
    - ticket
        - is done or not
    - information
    - shape (box, circle, paper)
    - shape color (common colors)
    - sigil (letter, shape)
    - sigil color
- Edge - A way to establish a connection between two nodes. One depends on another. One
    - dependency (A depends on B)
    - order (A before B (but not necessarily a dependency))
    - arrow type (a -> b, a <- b, a <-> b, a --- b, solid arrows or not, a - b, dots, hollow dots)
