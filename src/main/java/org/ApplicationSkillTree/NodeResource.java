package org.ApplicationSkillTree;

// import java.util.Optional;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

// import org.eclipse.microprofile.health.HealthCheck;
// import org.eclipse.microprofile.health.HealthCheckResponse;
// import org.eclipse.microprofile.health.Liveness;
// import org.eclipse.microprofile.health.Readiness;
// import org.eclipse.microprofile.health.HealthCheckResponse.State;


// @Readiness
// @Liveness
@Path("/node")
public class NodeResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "hello";
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/test")
    public String test() {
        return "{\"value\": \"test\"}";
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/health/status")
    public String status() {
        return "UP";
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/health/live")
    public String liveness() {
        return "alive";
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/health/ready")
    public String readiness() {
        return "ready";
    }

}