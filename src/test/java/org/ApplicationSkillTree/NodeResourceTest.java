package org.ApplicationSkillTree;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class NodeResourceTest {

    @Test
    public void testHelloEndpoint() {
        given()
          .when().get("/node")
          .then()
             .statusCode(200)
             .body(is("hello"));
    }

    @Test
    public void testStatus() {
        given()
          .when().get("/node/health/status")
          .then()
             .statusCode(200)
             .body(is("UP"));
    }

    @Test
    public void testReady() {
        given()
          .when().get("/node/health/ready")
          .then()
             .statusCode(200)
             .body(is("ready"));
    }

    @Test
    public void testLive() {
        given()
          .when().get("/node/health/live")
          .then()
             .statusCode(200)
             .body(is("alive"));
    }

}